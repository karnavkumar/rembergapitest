const { mongoose } = require('../../../configs');
const { Schema } = mongoose;

const Selections = {
  selection: Array,
};

module.exports = mongoose.model('selections', new Schema(Selections, { timestamps: true }));

const router = require('express').Router();

const { FakerController, AssetController } = require('./controllers');

router.get('/store', FakerController.storeFakeData);
router.get('/names', AssetController.Get);
router.put('/selection', AssetController.Update);
router.get('/selection', AssetController.GetSelections);
module.exports = router;
